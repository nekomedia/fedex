export default {
  server: {
    port: 3000 // default: 3000
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'fedex',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'preconnect', href:"https://fonts.googleapis.com"},
      { rel: 'preconnect', href:"https://fonts.gstatic.com", crossorigin: true},
      { rel: 'preconnect', href:"https://www.fedex.com", crossorigin: true},
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/npm/tailwindcss@2.1.2/dist/tailwind.min.css'},
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap'},
      { rel: 'preload', href: 'https://www.fedex.com/etc.clientlibs/clientlibs/fedex/images/resources/menu-sprite.png', as:'image'}
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: {
    dirs: [
      '~/components',
      '~/components/layout/',
      '~/components/sections/',
      '~/components/blocks/',
      '~/components/common/',
    ]
  },

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    [
      '@nuxtjs/firebase',
      {
        config: {
          apiKey: "AIzaSyC3jkcm2wz1HgBhcHdWDZCxBRcvYcGwFQw",
          authDomain: "fedex-ddezeeuw.firebaseapp.com",
          projectId: "fedex-ddezeeuw",
          storageBucket: "fedex-ddezeeuw.appspot.com",
          messagingSenderId: "15458685666",
          appId: "1:15458685666:web:2b3ac1db82355f217b04bb",
          measurementId: "G-ZWJT0J6WX5"
        },
        services: {
          auth: true,
          firestore: true,
          functions: true,
          storage: true,
          database: true,
          messaging: true,
          performance: true,
          analytics: true,
          remoteConfig: true
        }
      }
    ]
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },
  tailwindcss: {
    configPath: '~/tailwind.config.js',
    cssPath: '~/assets/css/tailwind.css'
  }
}
