# FedEX

#### Description:
This is an assessment for the A/B test project.

#### Urls:
- source:	https://bitbucket.org/nekomedia/fedex/src/develop/
- site:		http://ec2-52-58-39-232.eu-central-1.compute.amazonaws.com/ or http://3.121.127.54

while running locally:
- styles used: http://localhost:3000/_tailwind/

#### Cypress IO Test status
[![fedex](https://img.shields.io/endpoint?url=https://dashboard.cypress.io/badge/detailed/g4zgjn/develop&style=flat&logo=cypress)](https://dashboard.cypress.io/projects/g4zgjn/runs)

### Technologies used:
- Hosting				- Amazon AWS (EC2 Ubuntu instance with Elastic URL)
- Code hosting	- Bitbucket (GIT based with Cypress integration)
- Frontend			- framework: VueJS
- Serverside		- rendering through NUXTjs
- Database			- Google Firebase (Object based)
- Code checking - ESLINT / PRETTIER
- Testing				- Cypress IO (with dashboard and Bitbucket hooks)

### Build and serve the app
Now build your app with npm run build.

And serve it with `sudo pm2 start ./index.js --name FedEx`.
This starts the server in fork mode under root to open the server to port 80.

Check the status `pm2 ls`.
Check the logs with `pm2 log FedEx`.

_Your Nuxt application is now serving!_

### Scripts:
`npm run -`

- test 			- "cy:open",
- dev				- "nuxt",
- build			- "nuxt build",
- start			- "nuxt start",
- generate	- "nuxt generate"
