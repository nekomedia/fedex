// this is the main store where we keep track of the state.
// We use 'pure' functions to keep things simple and organized
// all mutations on data will be found here.

export const state = () => ({
  activeSection: 'create',
  showSummary: false,
  cases: {},
  regions: null,
  types: null
})

export const getters = {
  GET_CASE: (state) => (id) => {
    return state.cases.find(item => item.timestamp === id)
  }
}

export const mutations = {
  SET_REGIONS(state, commit) {
    state.regions = commit;
  },
  SET_TYPES(state, commit) {
    state.types = commit;
  },
  SET_SUMMARY_STATE(state, commit) {
    state.showSummary = commit;
  },
  SET_SECTION(state, commit) {
		state.activeSection = commit;
	},
  SET_CASES(state, commit) {
    state.cases = commit;
  },
  SET_CASE(state, commit) {
    state.cases[commit.id] = commit.case;
  },
}

export const actions = {
  async GET_CASES_FIRESTORE({ commit }) {
		const ref = this.$fire.firestore.collection('cases');
		try {
			await	ref.get().then(snap => {
				let cases = [];
				snap.forEach(doc => {
					cases.push(doc.data());
				})
				commit('SET_CASES', cases);
			});

		} catch (e) {
			return Promise.reject(e);
		}
	},
	async SET_CASE_FIRESTORE({commit, state, rootState }) {
    const abtest = rootState.abtest
    const timestamp = + new Date();
    const caseObj = {
			name: abtest.name,
			region: abtest.region,
			status:  abtest.status,
			type: abtest.type,
			text: abtest.text,
			format: abtest.format,
			source: abtest.source,
			target: abtest.target,
      created: timestamp
		}
		try {
			await this.$fire.firestore
			.collection('cases')
				.doc(String(timestamp))
				.set(caseObj);
      console.log('set case', caseObj);
			commit('SET_CASE', caseObj);
		} catch (e) {
			return Promise.reject(e);
		}
	},
	async DELETE_CASE_FIRESTORE({}, context) {
		const ref = this.$fire.firestore.collection('cases');
		try {
      await ref.doc(context)
      .delete()
      .then(function () {
          console.log("Document successfully deleted!");
      }).catch(
          function(error) {
          console.error("Error removing document: ", error);
      });
    } catch (e) {
      return Promise.reject(e);
    }
  }
}
