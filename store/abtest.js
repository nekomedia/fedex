// this is the A/B store where we keep track of the state on case level.
// We use 'pure' functions to keep things simple and organized
// all mutations on case data will be found here.

export const state = () => ({
	name: null,
	region: null,
	status: null,
	type: null,
	text: null,
	format: null,
	source: null,
	target: null
})

export const mutations = {
	SET_CASE_TO_DEFAULT(state) {
		state.name = null;
		state.region = null;
		state.status =  null;
		state.type = null;
		state.text = null;
		state.format = null;
		state.source = null;
		state.target = null;
	},
	SET_CASE_TYPE_SET_DEFAULT(state) {
		console.log('reset time');
		state.text = null;
		state.format = null;
		state.source = null;
		state.target = null;
	},
	SET_CASE_FIELD_VALUE(state, commit) {
		state[commit.field] = commit.value;
	}
}
