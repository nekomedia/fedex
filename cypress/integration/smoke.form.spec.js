/// <reference types="cypress" />
const D = new Date();
const dateString = D.getUTCDate() + '-' + (D.getUTCMonth() + 1) + '-' + D.getUTCFullYear() + ' - ' + D.getUTCHours() + ' ' + D.getUTCMinutes();
describe(`ABTest`, () => {
  Cypress._.each(['macbook-15', 'iphone-8'], viewport => {
    describe(`Run the tests on ${viewport === 'macbook-15' ? 'desktop' : 'mobile'}.`, () => {
      beforeEach( () => {
        cy.viewport(viewport);
      });

      context('Tab 1: Create a new A/B case', () => {
        it(`Should be able to reach the page`, () => {
          cy.viewport(viewport);
          cy.visit('http://localhost:3000').wait(1000);
          cy.get('#__layout').should('exist');
        })

        it('Should be able to present the Create new case form', () => {
          cy.get('form[data-test=form-abcase]').should('exist');
        });

        it('Should be able to fill in the form fields', () => {
          cy.get('input[name=name').type('CY Autom ' + viewport + ' ' + dateString);
          cy.get('select[name=region').select('1');
          cy.get('input[name=status]:eq(0)').click();
          cy.get('input[name=type]:eq(0)').click();
        });

        context('Should be able fill in the detailed media form fields', () => {
          it('Should be able to present the detailed media form fields', () => {
            cy.get('fieldset[data-test=field-set-details]').should('exist');
          });
          it('Should be able to fill in Case: Image', () => {
            cy.get('input[name=text').type('CY TEXT' + dateString);

            cy.log('test image jpg with wrong and correct input');
            cy.get('select[name=format').select('jpg');
            cy.get('input[name=source]').type('CY-file-format-wrong.png').blur()
              .next().invoke('attr','class').should('contain','text-red');
            cy.get('input[name=source]').clear().type('CY-file-format-correct.jpg');
            cy.get('input[name=target]').type('CY-some-container');

            cy.log('test image png with wrong and correct input');
            cy.get('select[name=format').select('png');
            cy.get('input[name=source]').clear().type('CY-file-format-wrong.jpg').blur()
              .next().invoke('attr','class').should('contain','text-red');
            cy.get('input[name=source]').clear().type('CY-file-format-correct.png').blur();

          cy.log('Form should be validated and ready to be submitted');
            cy.get('button[type=submit]').invoke('attr','class').should('contain','bg-yellow-500');
          })

          it('Should be able to fill in Case: Video', () => {
            cy.get('input[name=type]:eq(1)').click();
            cy.get('input[name=text').type('CY TEXT' + dateString);

            cy.log('test video mp4 with wrong and correct input');
            cy.get('select[name=format').select('MP4');
            cy.get('input[name=source]').type('CY-file-format-wrong.mov').blur()
              .next().invoke('attr','class').should('contain','text-red');
            cy.get('input[name=source]').clear().type('CY-file-format-correct.mp4');
            cy.get('input[name=target]').type('CY-some-container');

            cy.log('test video mov with wrong and correct input');
            cy.get('select[name=format').select('MOV');
            cy.get('input[name=source]').clear().type('CY-file-format-wrong.mp4').blur()
              .next().invoke('attr','class').should('contain','text-red');
            cy.get('input[name=source]').clear().type('CY-file-format-correct.mov').blur();

            cy.log('Form should be validated and ready to be submitted');
            cy.get('button[type=submit]').invoke('attr','class').should('contain','bg-yellow-500');
          })

          it('Should be able to fill in Case: Element BUTTON ', () => {
            cy.get('input[name=type]:eq(2)').click();
            cy.get('input[name=text').type('CY TEXT' + dateString);

            cy.log('test video mp4 with wrong and correct input');
            cy.get('select[name=format').select('button');
            cy.get('input[name=source]').type('http://www.fedex.com').blur()
              .next().invoke('attr','class').should('contain','text-red');
            cy.get('input[name=source]').clear().type('primary');
            cy.get('input[name=target]').type('CY-some-container');

            cy.log('test video mov with wrong and correct input');
            cy.get('select[name=format').select('link');
            cy.get('input[name=source]').clear().type('primary').blur()
              .next().invoke('attr','class').should('contain','text-red');
            cy.get('input[name=source]').clear().type('http://fedex.com').blur();

            cy.log('Form should be validated and ready to be submitted');
            cy.get('button[type=submit]').invoke('attr','class').should('contain','bg-yellow-500');
          })

          it('Should be able to view the summary modal of the new case', () => {
            cy.get('button[type=submit]').click();
            cy.get('div[data-test=modal-summary]').find('h2').contains('Summary')
            .parent().find('ul li:eq(0) > strong').contains('Name')
            .parent().find('span').contains('CY Autom ' + viewport + ' ' + dateString);

            cy.get('div[data-test=modal-summary] ul li:eq(4) strong').contains('Details of selected type:');
            cy.get('div[data-test=modal-summary] ul li:eq(5) span').contains('CY TEXT' + dateString);
          });

          it('Should be able to close and submit the summary modal', () => {
            cy.get('div[data-test=modal-summary] button.absolute').trigger('click').wait(50);
            cy.get('button[type=submit]').click();
            cy.get('div[data-test=modal-summary]').should('exist');
          });

        });
      });

      context('Tab 2: Save and filter the form', () => {
        it(' Save the results', () => {
          cy.get('button.secondary').click();
          cy.get('div[data-test=modal-summary] p[data-test=success-message]').contains('Do you want to see the cases list?');
          cy.get('button.secondary').click();
        })
      })
    });
  });


});
