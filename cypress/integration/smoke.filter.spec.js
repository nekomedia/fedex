/// <reference types="cypress" />

const D = new Date();
const dateString =
  D.getUTCDate() +
  '-' +
  (D.getUTCMonth() + 1) +
  '-' +
  D.getUTCFullYear() +
  ' - ' +
  D.getUTCHours() +
  ' ' +
  D.getUTCMinutes();
let viewport = 'macbook-15';
describe(`ABTest`, () => {
  Cypress._.each(['macbook-15', 'iphone-8'], viewport => {
    describe(`Run the tests on ${
      viewport === 'macbook-15' ? 'desktop' : 'mobile'
    }.`, () => {
      beforeEach(() => {
        cy.viewport(viewport);
      });

      context('Tab 2: Filter the results', () => {
        it(`Should be able to view the page`, () => {
          cy.viewport(viewport);
          cy.visit('http://localhost:3000').wait(1000);
          cy.get('#__layout').should('exist');
          cy.get('nav[data-test=nav-main] button:eq(1)').click();
          cy.get('div[data-test=section-list-cases]').should('exist');
        });

        it('Should be able to see the cases', () => {
          cy.get('ul[data-test=cases-list] li:eq(0)').should('exist');
        });

        it('Should be able to filter on region', () => {
          cy.get(
            'div[data-test=section-list-cases] select[name=region]'
          ).select('5');
          cy.get('ul[data-test=cases-list] li:eq(0)').should('not.exist');
          cy.get(
            'div[data-test=section-list-cases] select[name=region]'
          ).select('1');
          cy.get('ul[data-test=cases-list] > li:eq(1)')
            .find('span')
            .contains('Nice European inactive A/B case');
        });

        it('Should be able to filter on status', () => {
          cy.get(
            'div[data-test=section-list-cases] input[name=status]:eq(0)'
          ).click();
          cy.get('ul[data-test=cases-list] > li:eq(1)')
            .click()
            .find('ul')
            .contains('active');
          cy.get('ul[data-test=cases-list] > li:eq(1) > div').click();
        });

        it('Should be able to filter on type', () => {
          cy.get(
            'div[data-test=section-list-cases] input[name=type]:eq(2)'
          ).click();
          cy.get('ul[data-test=cases-list] > li')
            .eq(1)
            .click()
            .contains('CY Autom macbook-15 27-10-2021 - 16 9')
            .parent()
            .parent()
            .find('ul')
            .contains('element');
          cy.get('ul[data-test=cases-list] > li:eq(1) > div').click();
        });

        it('Should be able to reset the filters', () => {
          cy.get('div[data-test=section-list-cases] button').click();
          cy.get('ul[data-test=cases-list] > li:eq(3)')
            .find('span')
            .contains('Case Middle-East Video Inactive');
          cy.get('ul[data-test=cases-list] > li:eq(3)')
            .click()
            .find('ul')
            .then($item => {
              console.log('ul item', $item);
              cy.wrap($item).contains('inactive');
              cy.wrap($item).contains('video');
            });
          cy.get('ul[data-test=cases-list] > li:eq(3) > div').click();
        });
      });
    });
  });
});
