module.exports = {
  theme: {
    fontFamily: {
      fedex: ['Roboto', 'sans-serif'],
    },
    extend: {
      colors: {
        transparent: 'transparent',
        current: 'currentColor',
        fedex: '#4d148c',
        fedexDark: '#330466',
        fedexGray: '#efefef',
        fedexGrayDark: '#cfcfcf',
        fedexOrange: '#f74a00',
        fedexOrangeDark: '#d64304'
      },
      spacing: {
        '128': '32rem',
        '144': '36rem',
      },
      borderRadius: {
        '4xl': '2rem',
      }
    }
  },
  corePlugins: {
    textColor: true
  }
}
