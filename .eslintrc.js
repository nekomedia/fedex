module.exports = {
    env: {
      browser: true,
      node: true,
    },
    extends: ['prettier', 'prettier/vue', 'plugin:prettier/recommended', 'plugin:vue/essential', 'plugin:vuejs-accessibility/recommended'],
    // parserOptions: {
    //   parser: 'babel-eslint',
    // },

    plugins: ['vue', 'prettier'],
    rules: {
      'prettier/prettier': 'warn',
      'no-console': ['warn', { allow: ['warn', 'error'] }],
      'no-debugger': 'warn',
      'vuejs-accessibility/no-onchange': 'off',
      'no-template-curly-in-string': 0,
      quotes: ['warn', 'single', { avoidEscape: true, allowTemplateLiterals: true }],
      'template-curly-spacing': 0,
      indent: ['warn', 2, { SwitchCase: 1, ignoredNodes: ['TemplateLiteral'] }],
      'vuejs-accessibility/label-has-for': [
        2,
        {
          components: ['Label'],
          required: {
            some: ['nesting', 'id'],
          },
          allowChildren: false,
        },
      ],
    },
  };
